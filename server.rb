require 'webrick'

server = WEBrick::HTTPServer.new Port: ENV['PORT'] || 5000

server.mount_proc '/' do |request, response|
  response.body = 'René say: Hello, world!'
end

server.start
